<?php

namespace Pedrosaraiva\Custom;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;


class CustomServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot(): void
    {
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'pedrosaraiva');
        $this->loadViewsFrom(__DIR__.'/../resources/views/components', 'components');

        Blade::component('components::teste', 'teste');
        Blade::component('components::teste2', 'teste2');

        // $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadRoutesFrom(__DIR__.'/routes.php');

        // Publishing is only necessary when using the CLI.
        if ($this->app->runningInConsole()) {
            $this->bootForConsole();
        }
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__.'/../config/custom.php', 'custom');

        // Register the service the package provides.
        $this->app->singleton('custom', function ($app) {
            return new Custom;
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['custom'];
    }

    /**
     * Console-specific booting.
     *
     * @return void
     */
    protected function bootForConsole(): void
    {
        // Publishing the configuration file.
        $this->publishes([
            __DIR__.'/../config/custom.php' => config_path('custom.php'),
        ], 'custom.config');

        // Publishing the views.
        $this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/vendor/pedrosaraiva'),
        ], 'custom.views');

        // Publishing assets.
        /*$this->publishes([
            __DIR__.'/../resources/assets' => public_path('vendor/pedrosaraiva'),
        ], 'custom.views');*/

        // Publishing the translation files.
        /*$this->publishes([
            __DIR__.'/../resources/lang' => resource_path('lang/vendor/pedrosaraiva'),
        ], 'custom.views');*/

        // Registering package commands.
        // $this->commands([]);
    }
}
